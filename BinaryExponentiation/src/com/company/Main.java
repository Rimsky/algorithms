package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        Scanner scanner = new Scanner(new File("input.txt"));
        ArrayList<Integer> permutation = new ArrayList<Integer>();
        ArrayList<Integer> p1 = new ArrayList<Integer>();
        ArrayList<Integer> p2 = new ArrayList<Integer>();
        int size = scanner.nextInt(); //Читаем с файла размер перестановки
        for (int i = 0; i < size; i ++)
        {
            p1.add((scanner.nextInt()));
        }
        for (int i = 0; i < size; i ++)
        {
            p2.add((scanner.nextInt()));
        }
        //int n = scanner.nextInt(); //Читаем степень
       // int x = scanner.nextInt();
       /* for (int i = 0; i < size; i++)
        {
            permutation.add(scanner.nextInt()); //В цикле читаем элементы перестановки и добавляем их в массив
        }*/


       /* System.out.println("Перестановка: ");
        for(Integer i : permutation)
        {
            System.out.print(i+" ");
        }
        System.out.println();*/
       // System.out.println("Степень возведения: " + n);
        System.out.println();

        ArrayList<Integer> bits = new ArrayList<Integer>(); //Создаем массив битов
        //bits = toBinary(n); //Переводим степень возведения в двоичную систему счисления

        System.out.println("Двоичный код степени(инвертирован): ");
        for (Integer i : bits)
        {
            System.out.print(i);
        }
        System.out.println();
        System.out.println();
       // System.out.println("Результат возведения числа " + x + " в степень " + n + ": "  + LeftRightBinaryExponentiation(x, bits));
       // ArrayList<Integer> result = LeftRightBinaryExponentiation(permutation, bits);//Вызываем метод бинарного возведения
        ArrayList<Integer> result = PermutationsMultiply(p1,p2);
        System.out.println("Результат умножения перестановок: ");
        for ( Integer i : result)
        {
            System.out.print(i + " ");
        }
     /*   System.out.println("Результат возведения перестановки в " + n + " степень: ");
        for (Integer i : result)
        {
            System.out.print(i + " ");
        }*/

    }

    public static ArrayList<Integer> toBinary(int n) //Перевод в двоичный код
    {
        int i, b;
        ArrayList<Integer> bits = new ArrayList<>();
        while(n !=0)
        {
            b = n%2; //Остаток от деления на 2
            bits.add(b);
            n = n/2;
        }
        return bits;
    }
/*
    public static ArrayList<Integer> LeftRightBinaryExponentiation(ArrayList<Integer> permutation, ArrayList<Integer> bits)
    {
        ArrayList<Integer> product = permutation;
        for (int i = bits.size()-2; i >= 0; i--)
        {
            product = PermutationsMultiply(product, product);
            if (bits.get(i) == 1)
            {
                product = PermutationsMultiply(product, permutation);
            }
        }

        return product;
    }*/

    public static int LeftRightBinaryExponentiation(int  permutation, ArrayList<Integer> bits)
    {
        int product = permutation;
        for (int i = bits.size()-2; i >= 0; i--)
        {
            product = product * product;
            if (bits.get(i) == 1)
            {
                product = product * permutation;
            }
        }

        return product;
    }

    //Функция умножения перестановок
    public static ArrayList<Integer> PermutationsMultiply(ArrayList<Integer> p1, ArrayList<Integer> p2)
    {
        ArrayList<Integer> result = new ArrayList<>(p1);
        for (int i = 0; i < p2.size(); i++)
        {
            for (int j = 0; j < p2.size(); j++)
            {
                if (i+1==p2.get(j))
                {
                    for (int k = 0; k < p1.size(); k++)
                    {
                        if ((j+1)==p1.get(k))
                        {
                            result.set(k, p2.get(j));
                        }
                    }
                }
            }
        }
        return result;
    }

}
