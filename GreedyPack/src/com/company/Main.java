package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

    public static int W = 0;

    static class Item implements Comparable<Item> {

        int cost, weight;

        public Item(int cost, int weight) {
            this.cost = cost;
            this.weight = weight;
        }


        @Override
        public int compareTo(Item o) {
            long r1 = cost * o.weight;
            long r2 = o.cost * weight;
            return -Long.compare(r1, r2);
        }

        @Override
        public String toString() {
            return "Item{" +
                    "cost=" + cost +
                    ", weight=" + weight +
                    '}';
        }
    }

    private void run(Item[] items) throws FileNotFoundException {

        Arrays.sort(items);

        double result = 0;

        for (Item item : items) {
            if (item.weight <= W) {
                result += item.cost;
                W -= item.weight;
            } else {
                result += (double) item.cost * W / item.weight;
                break;
            }
        }

        System.out.println("Максимальная стоимость предметов: " + result);

    }

    public static void main(String[] args) throws FileNotFoundException {

        Scanner input = new Scanner(new File("input.txt"));
        int n = input.nextInt();
        W = input.nextInt();
        Item[] items = new Item[n];
        for (int i = 0; i < n; i++) {
            items[i] = new Item(input.nextInt(), input.nextInt());
        }

        for (int i = 0; i < n; i++) {
            System.out.println(items[i]);
        }

        long startTime = System.nanoTime();
        new Main().run(items);
        long finishTime = System.nanoTime();
        System.out.println("Время работы жадного алгоритма для рюкзака:" + " " + (finishTime - startTime) + " ns");
    }
}
