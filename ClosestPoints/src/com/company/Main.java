package com.company;

import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    static  long [][] timeMassivBF = new long[10][100];
    static  long [][] timeMassivDec = new long[10][100];
    static double m = Integer.MAX_VALUE;

    public static void main(String[] args) throws FileNotFoundException {

        long averageTimeDecomposition = 0;
        long averageTimeBrute = 0;
        for (int j = 0; j < 100; j++) {
            ArrayList<Point> point = new ArrayList<>();
            int min = 0;
            int max = 1000;
            HashSet<Integer> ptX = new HashSet<>();
            for(int k = 1; k <=10; k++) {
                for (int i = 0; i < k*10; i++) {
                    Random rnd = new Random();
                    int rndX = min + rnd.nextInt(max - min + 1);
                    int rndY = min + rnd.nextInt(max - min + 1);
                    if (!ptX.contains(rndX)) {
                        ptX.add(rndX);
                        point.add(new Point(rndX, rndY, point.size()));
                        System.out.println(point.size() + ": " + " X: " + rndX + " Y: " + rndY);
                    } else {
                        i--;

                    }

                }
            }

            long startTime = System.nanoTime();
            BruteForceClosestPoints a = new BruteForceClosestPoints();
            a.BruteFindClosestPoints(point);
            a.show();
            averageTimeBrute += System.nanoTime() - startTime;


            Collections.sort(point, Point.COMPARE_BY_X);
            ArrayList pointX = new ArrayList<>(point);
            Collections.sort(point, Point.COMPARE_BY_Y);
            ArrayList pointY = new ArrayList<>(point);

            startTime = System.nanoTime();
            DecompositionClosestPoints b = new  DecompositionClosestPoints();
            b. DecompositionClosestPoints(pointX, pointY);
            averageTimeDecomposition += System.nanoTime() - startTime;
            b.show();

            System.out.println("---------------------------------------");
        }
        System.out.println("Среднее время грубый: " + (averageTimeBrute ));
        System.out.println("Среднее время декомпозиции: " + (averageTimeDecomposition ));

        long beginBF, endBF, beginDec, endDec;
       for (int i = 0; i < 100; i++) {
           for (int j = 0; j < 10; j++) {
                //randomPoints = getListPointsInSquareArea(new Point(10, 50), new Point(50, 10), (j+1)*10 );

               Point leftUp = new Point(10, 50);
               Point rightDown = new Point(50, 10);
               ArrayList<Point> pointListRet = new ArrayList<>();
               Random rnd = new Random(System.currentTimeMillis());
               for (int k = 0; k < (j+1)*10; k++) {
                   pointListRet.add(new Point(leftUp.getX() + rnd.nextInt((int) (rightDown.getY() + 1)),
                           leftUp.getY() + rnd.nextInt((int) (rightDown.getY() + 1)), k));
               }
              // return pointListRet;

                beginBF = System.nanoTime();
                BruteForceClosestPoints bfRand = new BruteForceClosestPoints();
                bfRand.BruteFindClosestPoints(pointListRet);
                endBF = System.nanoTime();

                System.out.println("Время грубого для " + (j+1)*10 + " точек: " + (endBF-beginBF) + " ns");


                DecompositionClosestPoints decRand = new DecompositionClosestPoints();
                Collections.sort(pointListRet, Point.COMPARE_BY_X);
                ArrayList pointX = new ArrayList<>(pointListRet);
                Collections.sort(pointListRet, Point.COMPARE_BY_Y);
                ArrayList pointY = new ArrayList<>(pointListRet);
                beginDec = System.nanoTime();
                decRand.DecompositionClosestPoints(pointX, pointY);
                endDec = System.nanoTime();
                System.out.println("Время декомпозиции для " + (j+1)*10 + " точек: " + (endDec-beginDec) + " ns");
                System.out.println();

               timeMassivBF[j][i]= endBF-beginBF;
               timeMassivDec[j][i] = endDec-beginDec;
           }
       }

        for (int i =0; i<10; i++){
            System.out.println("AVG (BruteForce) "+ i +" : "+ Arrays.stream(timeMassivBF[i]).average());
        }

        for (int i =0; i<10; i++){
            System.out.println("AVG (Decomposition)"+ i +" : "+ Arrays.stream(timeMassivDec[i]).average());
        }


    }

    private static ArrayList<Point> getListPointsInSquareArea(Point leftUp, Point rightDown, int count) {
        ArrayList<Point> pointListRet = new ArrayList<>();
        Random rnd = new Random(System.currentTimeMillis());
        for (int i = 0; i < count; i++) {
            Point newPoint = new Point(new Point(leftUp.getX() + rnd.nextInt((int) (rightDown.getY() + 1)),
                    leftUp.getY() + rnd.nextInt((int) (rightDown.getY() + 1)), i));
            if(!pointListRet.contains(newPoint))
            {pointListRet.add(newPoint);}
        }
        return pointListRet;
    }
}
