package com.company;

import java.util.ArrayList;
import static java.lang.Math.abs;

public class DecompositionClosestPoints {

    int index1 = -1, index2 = -1;
    int index3 = -1, index4 = -1;
    double m = Integer.MAX_VALUE;
    ArrayList<Integer> index = new ArrayList<>();

    double DecompositionClosestPoints(ArrayList<Point> pointX, ArrayList<Point> pointY) {

        int length = pointX.size();
        if (length <= 3) {
            BruteForceClosestPoints a = new BruteForceClosestPoints();
           double dist = a.BruteFindClosestPoints(pointX);
            index1 = a.getIndex1();
            index2 = a.getIndex2();
            return dist;
        }

        int mid = length / 2;

        Point midPoint = new Point(pointX.get(mid));

        ArrayList<Point> Pxl = new ArrayList<>(pointX.subList(0, mid + 1));
        ArrayList<Point> Pxr = new ArrayList<>(pointX.subList(mid + 1, length));

        ArrayList<Point> Pyl = new ArrayList<>();
        ArrayList<Point> Pyr = new ArrayList<>();
     //   System.out.println("Lenght " + length);
        for (int i = 0; i < length; i++) {
            if (pointY.get(i).getX() <= midPoint.getX()) {
                Pyl.add(pointY.get(i));
            } else {
                Pyr.add(pointY.get(i));
            }
        }

        DecompositionClosestPoints dl_ = new DecompositionClosestPoints();
        DecompositionClosestPoints dr_ = new DecompositionClosestPoints();

        double dl = dl_.DecompositionClosestPoints(Pxl, Pyl);
        double dr = dr_.DecompositionClosestPoints(Pxr, Pyr);
        double d = min(dl,dr);

       if (dl < dr) {
            index1 = dl_.getIndex1();
            index2 = dl_.getIndex2();
            d = dl;
        } else {
            index1 = dr_.getIndex1();
            index2 = dr_.getIndex2();
            d = dr;
        }

        ArrayList<Point> strip = new ArrayList<>();
        int j = 0;
        for (int i = 0; i < pointY.size(); i++) {
            if (abs(pointY.get(i).getX() - midPoint.getX()) < d) {
                strip.add(j, pointY.get(i));
                j++;
            }
        }

        m = min(d, stripClosest(strip,d));
        return m;
    }

    double stripClosest(ArrayList<Point> strip, double d)
    {
        double min = d; 
        for (int i = 0; i < strip.size(); i++) {
            for (int j = i + 1; j < strip.size() && (strip.get(j).getY() - strip.get(i).getY()) < min; ++j)
                if (dist(strip.get(i), strip.get(j)) < min)
                    min = dist(strip.get(i), strip.get(j));
        }

        return min;
    }

    public int getIndex1() {
        return index1;
    }

    public int getIndex2() {
        return index2;
    }

    void show() {
        System.out.println("Ближайшие точки (Decomposition): " + (index1) + " и " + (index2) + " с расстоянием: " + m);
    }

    double min(double x, double y) {
        return (x < y) ? x : y;
    }

    public double dist(Point p1, Point p2){
        return  Math.sqrt((p1.getX() - p2.getX())*(p1.getX() - p2.getX()) + (p1.getY() - p2.getY())*(p1.getY() - p2.getY()));

    }
}
