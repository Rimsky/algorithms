package com.company;

import java.util.ArrayList;

public class BruteForceClosestPoints {
    double min;
    ArrayList<Integer> index = new ArrayList<>();
    int index1 = -1, index2 = -1;

    double BruteFindClosestPoints(ArrayList<Point> points) {
         min = Integer.MAX_VALUE;

        double d;
        for (int i = 0; i < points.size() - 1; i++) {
            for (int j = i + 1; j < points.size()&&j!=i; j++) {

                d = (points.get(j).getX() - points.get(i).getX())*(points.get(j).getX() - points.get(i).getX())
                        + (points.get(j).getY() - points.get(i).getY())*(points.get(j).getY() - points.get(i).getY());

                if (d < min) {
                    min = d;
                    index1 = points.get(i).getID();
                    index2 = points.get(j).getID();
                }

            }

        }
        min = Math.sqrt(min);

        return min;
    }

    public ArrayList<Integer> getIndex() {
        return index;
    }

    public int getIndex1() {
        return index1;
    }

    public int getIndex2() {
        return index2;
    }

    void show() {
        System.out.println("Ближайшие точки (BruteForce): " + (index1) + " и " + (index2) + " с расстоянием: " + min);
    }
}
