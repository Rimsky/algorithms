package com.company;

public class HashMap { //открытая адресация

    private final static int TABLE_SIZE = 128; //задаем размерность таблицы

    HashEntry[] table; //создаем переменную для хранения ссылки на массив экземпляров класса HashEnrty

    public HashMap() { //Конструктор HashMap, который создает таблицу размерности TABLE_SIZE и заполняет ее пустыми значениями
        table = new HashEntry[TABLE_SIZE]; //создаем экземпляр таблицы и заносим ссылку на него в переменную table
        for (int i = 0; i < TABLE_SIZE; i++)
            table[i] = null; //заполняем каждую ячейку таблицы пустыми значенями
    }
  /*
        Пока хэш не равен "начальному" и если значение в таблице по ключу hash равна "удаленному" т.е. -1,
        или  если значение в таблице по ключу hash не равно null, совершаем проверку:
        если initialHash = -1, то initialHash = hash, затем "сдвигаем" хэш на единицу
        * */

    public int get(int key) { //метод для получения значения из таблицы по ключу

        int hash = (key % TABLE_SIZE);
        int initialHash = -1;


        while (hash != initialHash
                && (table[hash] == DeletedEntry.getUniqueDeletedEntry() || table[hash] != null
                && table[hash].getKey() != key)) {
            if (initialHash == -1)
                initialHash = hash;

            hash = (hash + 1) % TABLE_SIZE; //линейный поиск
        }

        if (table[hash] == null || hash == initialHash)  //если значение по ключу hash равно null или initalHash,
            //то возвращаем -1
            return -1;

        else //иначе возвращаем нужное значение

            return table[hash].getValue();
    }




    public void put(int key, int value) { //метод для вставки значения по ключу
        int compare = 0; //счетчик сравнений
        int hash = (key % TABLE_SIZE); //хешируем ключ
        int initialHash = -1; //значение, которое помогает понять, был ли захеширован ключ или нет
        int indexOfDeletedEntry = -1; //индекс удаленной записи
        /* Аналогичная проверка*/
        //Если мы имеем зашихешированный ключ, мы ищем ему свободный слот в таблице
        while (hash != initialHash
                && (table[hash] == DeletedEntry.getUniqueDeletedEntry() || table[hash] != null)
                )
        {
            if (initialHash == -1)
            {
                initialHash = hash;
            }

            if (table[hash] == DeletedEntry.getUniqueDeletedEntry()) //если значение по hash является удалнной записью
            {
                indexOfDeletedEntry = hash; //то присваиваем индексу удаленной записи наш хэщ
            }

            hash = (hash + 1) % TABLE_SIZE; //сдвигаемся
        }

        //Если ячейка пуста, кладем в нее новую запись (для ячеек, к которым применялась операция remove)
        if ((table[hash] == null || hash == initialHash)  //если ячейка пустая, или хэш "начальный"
                && indexOfDeletedEntry != -1) //и индекс удаленной записи не равен -1, т.е. запись была удалена
        {
            //count++;
            table[indexOfDeletedEntry] = new HashEntry(key, value); //вставляем на место удаленной записи новую пару
        }
        //ключ - значение

        else if (initialHash != hash) {
           // count++;//иначе если хэш не является "начальным"
        /*    if (table[hash] != DeletedEntry.getUniqueDeletedEntry() //если значение по хэшу не является удаленной записью
                    && table[hash] != null && table[hash].getKey() == key) //и ячейка не пуста и хэшированный ключ совпадает
                //с вновь введенным, то обновляем ячейку по ключу hash
                table[hash].setValue(value);*/
          //  else
                table[hash] = new HashEntry(key, value); //иначе - просто создаем новую запись
        }
        System.out.println("Число сравнений при коллизии: " + compare);

    }

    public void remove(int key) {

        //ряд аналогичных действий
        int hash = (key % TABLE_SIZE);

        int initialHash = -1;

        while (hash != initialHash

                && (table[hash] == DeletedEntry.getUniqueDeletedEntry() || table[hash] != null

                && table[hash].getKey() != key)) {

            if (initialHash == -1)

                initialHash = hash;

            hash = (hash + 1) % TABLE_SIZE;

        }

        if (hash != initialHash && table[hash] != null) //если хэш не "начальный" и ячейка не пуста, то удаляем
            //запись по ключу hash
            table[hash] = DeletedEntry.getUniqueDeletedEntry();

    }


}
