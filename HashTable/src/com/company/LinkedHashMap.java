package com.company;

public class LinkedHashMap { //метод цепочек

    private final static int TABLE_SIZE = 128; //задаем размерность таблицы
    LinkedHashEntry[] table; //создаем переменную для хранения ссылки на массив записей

    LinkedHashMap() { //конструктор для создания таблицы

        table = new LinkedHashEntry[TABLE_SIZE]; //создаем экземпляр новой таблицы

        for (int i = 0; i < TABLE_SIZE; i++) //заполняем пустыми значениями

            table[i] = null;
    }

    public int compares = 0;

    public int get(int key) { //метод для получения значения по ключу

        int hash = (key % TABLE_SIZE);

        if (table[hash] == null) //если ячейка по hash пустая, то возвращаем -1

            return -1;

        else { //иначе

            LinkedHashEntry entry = table[hash]; //копируем значение из таблицы в отедльную переменную

            while (entry != null && entry.getKey() != key) //пока запись не пуста и ключ записи не равен переданному ключу

                entry = entry.getNext(); //переходим к следующей записи

            if (entry == null) //если запись пуста, то возвращаем -1

                return -1;

            else

                return entry.getValue(); //иначе, возвращаем значение

        }

    }


    public void put(int key, int value) { //метод для вставки новой записи по ключу

        int hash = (key % TABLE_SIZE); //хэшируем ключ

        if (table[hash] == null) //если ячейка пустая, то просто создаем новую запись

            table[hash] = new LinkedHashEntry(key, value);

        else { //иначе

            LinkedHashEntry entry = table[hash]; //копируем значение из таблицы в отдельную переменную

            while (entry.getNext() != null && entry.getKey() != key) //Пока следующая запись НЕ пустая и значение ключа записи не равно переданному ключу
            {
                entry = entry.getNext(); //переходим к следующей записи
                compares++;
            }

            if (entry.getKey() == key) //если ключ записи равен переданному ключу
            //то вставляем в это место новое значение (обновляем значение)
                entry.setValue(value);

            else
            //иначе устанавливаем ссылку на новую запись
                entry.setNext(new LinkedHashEntry(key, value));

            System.out.println("Число сравнений при коллизии в методе цепочек: " + compares);

        }
    }

    public void remove(int key) { //метод для удаления значения  по ключу

        int hash = (key % TABLE_SIZE);

        if (table[hash] != null) { //если запись не пустая, то обнуляем

            LinkedHashEntry prevEntry = null; //ссылка на предыдущая запись, инициализируем пустым значением

            LinkedHashEntry entry = table[hash]; //копируем значение из таблицы в отдельную переменную

            while (entry.getNext() != null && entry.getKey() != key) {
            //пока следующая запись не пустая и ключ записи не совпадает с переданным ключом
                prevEntry = entry; //в ссылку на предыдущаю запись добавляем текущую запись

                entry = entry.getNext(); //переходим к следующей записи

            }

            if (entry.getKey() == key) { //если ключ записи совпадает с текущим ключем

                if (prevEntry == null) //если ссылка на предыдущую запись пустая
                //то добавляем следующую запись в таблицу по хэшу
                    table[hash] = entry.getNext();

                else //иначе, "смещаем запись". т.е. на место предыдущей записи вставляем следующую запись

                    prevEntry.setNext(entry.getNext());
            }

        }

    }
}
