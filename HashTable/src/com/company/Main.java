package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        LinkedHashMap map = new LinkedHashMap();
        map.put(0, 1);
        map.put(1, 2);
        map.put(2, 3);
        map.put(1, 7);
        map.put(3, 1);

        System.out.println(map.get(0));
        System.out.println(map.get(1));
        System.out.println(map.get(2));
        System.out.println(map.get(1));
        System.out.println(map.get(3));
        Scanner input1 = new Scanner(new File("input1.txt")); //открываем первый файл
        Scanner input2 = new Scanner(new File("input2.txt")); //открываем второй файл
        HashMap map1 = new HashMap(); //создаем экземпляр HashMap
        LinkedHashMap map2 = new LinkedHashMap(); //Создаем экземпля LinkedHashMap
        int n1 = input1.nextInt(); //Размер первого файлы
        int n2 = input2.nextInt(); //размер второго файла

        for (int i = 0; i < n1; i++) //считываем данные из первого файла
        {
            int key = input1.nextInt();
            int value = input1.nextInt();
            map1.put(key, value); //добавляем в наш HashMap
        }

        //Тестируем открытую адресацию - выводим записи по ключам
        /*Вывод:
                Key: 1 Value: 2
                Key: 3 Value: 2
                Key: 4 Value: 3*/

        System.out.println("Key: 1" + " Value: " + map1.get(1));
        System.out.println("Key: 3" + " Value: " + map1.get(3));
        System.out.println("Key: 4" + " Value: " +map1.get(4));
        //Удаляем значение по ключу 3
        map1.remove(3);

        /*Вывод:    Key: 1 Value: 2
                    Key: 3 Value: -1
                    Key: 4 Value: 3*/

        System.out.println("__________________________________");
        System.out.println("Key: 1" + " Value: " + map1.get(1));
        System.out.println("Key: 3" + " Value: " +map1.get(3));
        System.out.println("Key: 4" + " Value: " +map1.get(4));
        System.out.println("__________________________________");
        //вставляем значение 12 по ключу 3
        map1.put(3, 12);
        //Выводим значение по ключу 3 - 12
        System.out.println(map1.get(3));

        for (int i = 0; i < n2; i++)//считываем данные из второго файла
        {
            int key = input2.nextInt();
            int value = input2.nextInt();
            map2.put(key, value); //добавляем  данные в наш LinkedHashMap
        }

        //Тестируем метод цепочек(прямого связывания) - выводим записи по ключам
       /* Вывод:    Key: 1 Value: 4
                    Key: 8 Value: 88
                    Key: 5 Value: 11
                    Key: 4 Value: 2
                    Key: 5 Value: 11*/
        System.out.println("__________________________________");
        System.out.println("Key: 1" + " Value: " + map2.get(1));
        System.out.println("Key: 8" + " Value: " + map2.get(8));
        System.out.println("Key: 5" + " Value: " + map2.get(5));
        System.out.println("Key: 4" + " Value: " + map2.get(4));
        System.out.println("Key: 5" + " Value: " + map2.get(5));
        System.out.println("__________________________________");
        //удаляем значение по ключу 5
        map2.remove(5);

        /*Вывод:    Key: 1 Value: 4
                    Key: 8 Value: 88
                    Key: 5 Value: -1
                    Key: 4 Value: 2
                    Key: 5 Value: -1*/

        System.out.println("Key: 1" + " Value: " + map2.get(1));
        System.out.println("Key: 8" + " Value: " + map2.get(8));
        System.out.println("Key: 5" + " Value: " + map2.get(5));
        System.out.println("Key: 4" + " Value: " + map2.get(4));
        System.out.println("Key: 5" + " Value: " + map2.get(5));
    }
}
