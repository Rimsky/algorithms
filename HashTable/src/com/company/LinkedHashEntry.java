package com.company;

public class LinkedHashEntry {

    private int key; //КЛЮЧ
    private int value; //значение
    private LinkedHashEntry next; //ссылка на следующую запись

    LinkedHashEntry(int key, int value) { //конструктор для создания новой записи
        this.key = key;
        this.value = value;
        this.next = null;
    }


    public int getValue() { //метод для получения значения
        return value;
    }


    public void setValue(int value) { //метод для установки значения
        this.value = value;
    }


    public int getKey() { //метод для получения ключа
        return key;
    }


    public LinkedHashEntry getNext() {//получаем следующую запись
        return next;
    }


    public void setNext(LinkedHashEntry next) { //устанавливаем ссылку на следующую запись
        this.next = next;
    }
}
