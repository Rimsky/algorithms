package com.company;

public class DeletedEntry extends HashEntry { //Класс для удаленных записей, расширяет HashEntry

    private static DeletedEntry entry = null; //создаем пустую ссылку на экземпляр класса

    private DeletedEntry() { //конструктор для удаления записи(создает запись с пометкой null)
        super(-1, -1);
    }

    public static DeletedEntry getUniqueDeletedEntry() { //метод, возвращающий удаленную запись

        if (entry == null) //Если запись пуста (равна null)
            entry = new DeletedEntry(); //создаем новый экземпляр удаленной записи
        return entry; //возвращаем его
    }
}
