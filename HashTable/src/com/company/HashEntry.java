package com.company;

public class HashEntry {

    private int key; //переменная ключа
    private int value; //переменная значения

    HashEntry(int key, int value) { //конструктор, создающий запись

        this.key = key; //инициализируем ключ
        this.value = value; //инициализируем значение

    }

    //далее создаем геттеры и сеттеры, для доступа и взаимодействия с нашими приватными полями(ключ значение)

    public int getValue()  //получить значение
    {
        return value;
    }


    public void setValue(int value) { //установить значение
        this.value = value;
    }

    public int getKey() { //получить ключ
        return key;
    }
}
