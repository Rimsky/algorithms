package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static final String GENES = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890, .-;:_!\"#%&/()=?@${[]}";
    private static final int POPULATION_SIZE = 100;
    public static String TARGET = "";

    public static char mutatedGenes()
    {
        int len = GENES.length();
        int r = getRandom(0, len-1);
        return GENES.charAt(r);
    }

    public static String createGenome()
    {
        int len = TARGET.length();
        String genome = "";
        for(int i = 0;i<len;i++)
            genome += mutatedGenes();
        return genome;
    }

    public static int getRandom(int start, int end)
    {
        Random random = new Random();
        int range = (end - start)+1;
        int rand = start + (Math.abs(random.nextInt()) % range);
        return rand;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        TARGET = scanner.nextLine();

        int generation = 0;

        ArrayList<Individual> population = new ArrayList<>();
        boolean found = false;

        for(int i = 0;i<POPULATION_SIZE;i++)
        {
            String genome = createGenome();
            population.add(new Individual(genome));
        }

        while(!found)
        {
            Collections.sort(population);

            if(population.get(0).getFitness() <= 0)
            {
                found = true;
                break;
            }

            ArrayList<Individual> newGeneration = new ArrayList<>();

            // 10% самых приспособленных идут в следующее поколение
            int s = (10*POPULATION_SIZE)/100;

            for(int i = 0;i<s;i++)
            {
                newGeneration.add(population.get(i));
            }

            // остальных скрещиваем
            s = (90*POPULATION_SIZE)/100;
            for(int i = 0;i<s;i++)
            {
                int len = population.size();
                int r = getRandom(0, 50);
                Individual parent1 = population.get(r);
                r = getRandom(0, 50);
                Individual parent2 = population.get(r);
                Individual offspring = parent1.mate(parent2);
                newGeneration.add(offspring);
            }
            population.clear();
            population.addAll(newGeneration);
            System.out.println("Generation: " + generation);
            System.out.println("String: " + population.get(0).getChromosome());
            System.out.println("Fitness: " + population.get(0).getFitness());
            generation++;
        }
        System.out.println("Generation: " + generation);
        System.out.println("String: " + population.get(0).getChromosome());
        System.out.println("Fitness: " + population.get(0).getFitness());
    }

}

