package com.company;

import static com.company.Main.TARGET;
import static com.company.Main.getRandom;
import static com.company.Main.mutatedGenes;

public class Individual implements Comparable<Individual>
{
    private String chromosome;
    private int fitness;

    public Individual(String chromosome)
    {
        this.chromosome = chromosome;
        this.fitness = calculateFitness();
    }

    public int calculateFitness() //вычисляем приспособленность
    {
        char[] chrom = chromosome.toCharArray();
        char[] target = TARGET.toCharArray();
        int len = TARGET.length();
        int fitness = 0;
        for(int i = 0; i < len; i++)
        {
            if(chrom[i] != target[i])
            {
                fitness++;
            }
        }
        return fitness;
    }

    public Individual mate(Individual parent) //скрещивание
    {
        String childChromosome = "";

        int len = chromosome.length();
        char[] chrom = chromosome.toCharArray();
        char[] parentChrom = parent.chromosome.toCharArray();

        for(int i = 0;i<len;i++)
        {
            //генерируем вероятность
            double p = getRandom(0, 100)/100;

            //если вероятность меньше чем 0.45, вставляем ген от первого родителя
            if(p < 0.45)
                childChromosome += chrom[i];


            //если вероятность между 0.45 и 0.90, то от второго
            else if(p < 0.90)
                childChromosome += parentChrom[i];

            //иначе, проводим мутацию
            else
                childChromosome += mutatedGenes();
        }

        return new Individual(childChromosome);
    }

    public int getFitness() {
        return fitness;
    }

    public String getChromosome() {
        return chromosome;
    }

    public void setChromosome(String chromosome) {
        this.chromosome = chromosome;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    @Override
    public int compareTo(Individual individual) {
        return Integer.compare(fitness, individual.getFitness());
    }
}
